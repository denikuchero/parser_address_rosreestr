import requests as requests
import urllib3

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

cadnumber = '77:07:0007003:53'
url = f'https://rosreestr.gov.ru/api/online/fir_objects/'

def get_address(cadnumber):
    address = requests.get(url + cadnumber, verify=False)
    if address.status_code == 200:  # сделали успешный запрос
        for item in address.json():
            if item.get('srcObject') == len(address.json()) and item.get('addressNotes'):
                address = item.get('addressNotes')

    else:
        address = f'Кадастровый номер: <b>{cadnumber}</b>\nАдрес: по этому ' \
                 f'кадастровому номеру адрес не найден, проверьте правильность кадастрового номера'

    return  address

if __name__ == '__main__':

    address = get_address(cadnumber)
    print(address)